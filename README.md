# Knotenkatalog #

NEU: Jetzt auch optimiert fuer Mobilgeräte

Mehr als 600 Downloads in der ersten Woche, mehr als 1400 Downloads insgesamt - eurer Interesse freut uns.

## Was ist der Knotenkatalog? ##

* Ein Einstieg ins Schlingenlegen
* Für das Schlingen-Übungsgebiet SBB-Klettergarten Pirna-Liebethal

## Wie kann ich ihn nutzen? ##

* Als [PDF-Dokument für Mobilgeräte herunterladen](https://bitbucket.org/lechris/knotenkatalog-liebethal/downloads/knotenkatalog_phone.pdf) (Version vom 10.9.18)
* Als [PDF-Dokument für den Rechner herunterladen](https://bitbucket.org/lechris/knotenkatalog-liebethal/downloads/knotenkatalog_liebethal.pdf) (Version vom 10.9.18)

* Durch Klicken auf die Schlingenstelle im Routenbild gelangt man zum
  Detailbild und durch Klicken auf dem Detailbild zurück zum Routenbild.
  Textlinks sind in blauer Schrift mit Pfeil gekennzeichnet.
* Bitte beachte Beachte Haftungsausschluss und Sicherheitshinweise!

## Wie kann ich mitmachen? ##

* Der Knotenkatalog wird ständig weiterentwickelt
* Kritik, Rückmeldungen, Korrekturen und Zuarbeit sind willkommen - bitte nutzt unser [Ticket-Sytem](https://bitbucket.org/lechris/knotenkatalog-liebethal/issues?status=new&status=open) dafür
